package com.fragment.kevinalejandrocastano.tabstolbar.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fragment.kevinalejandrocastano.tabstolbar.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SeconFragment extends Fragment {


    public SeconFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_secon, container, false);
        return view;
    }

}
