package com.fragment.kevinalejandrocastano.tabstolbar.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.fragment.kevinalejandrocastano.tabstolbar.Fragments.FirstFragment;
import com.fragment.kevinalejandrocastano.tabstolbar.Fragments.SeconFragment;
import com.fragment.kevinalejandrocastano.tabstolbar.Fragments.ThirdFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {

    private int numberOfTabs;

    public PagerAdapter(FragmentManager fm, int numberOfTabs) {
        super(fm);
        this.numberOfTabs = numberOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new FirstFragment();
            case 1:
                return new SeconFragment();
            case 2:
                return new ThirdFragment();
            default:
                return null;

        }

    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
